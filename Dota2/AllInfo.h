//
//  AllInfo.h
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-05.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AllInfo : NSObject

@property (nonatomic) NSDictionary *theHeroes;
@property (nonatomic) NSDictionary *theItems;
@property (nonatomic) NSMutableArray *allHeroNames;
@property (nonatomic) NSMutableArray *allItemNames;
@property (nonatomic) NSDictionary *theMatches;
+ (id)infoManager;
@end
