//
//  AllInfo.m
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-05.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "AllInfo.h"

@implementation AllInfo

@synthesize theHeroes;
@synthesize theItems;

+ (id)infoManager {
    static AllInfo *sharedInfo = nil;
    @synchronized(self) {
        if (sharedInfo == nil)
            sharedInfo = [[self alloc] init];
    }
    return sharedInfo;
}

@end
