//
//  HeroDetailViewController.h
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-09.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeroDetailViewController : UIViewController

@property (nonatomic) IBOutlet UILabel *atkTypeLabel;
@property (nonatomic) IBOutlet UILabel *rolesLabel;
@property (nonatomic) IBOutlet UITextView *bioTextView;
@property (weak, nonatomic) IBOutlet UIImageView *heroImage;

@end
