//
//  HeroDetailViewController.m
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-09.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "HeroDetailViewController.h"

@interface HeroDetailViewController ()

@property BOOL allowAnimation;

@end

@implementation HeroDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidLayoutSubviews {
    
    CGPoint atkOrigin = CGPointMake(self.atkTypeLabel.center.x, self.atkTypeLabel.center.y);
    CGPoint atkStartPos = CGPointMake(self.view.frame.size.width + self.atkTypeLabel.frame.size.width, self.atkTypeLabel.center.y);
    
    CGPoint roleOrigin = CGPointMake(self.rolesLabel.center.x, self.rolesLabel.center.y);
    CGPoint roleStartPos = CGPointMake(self.view.frame.size.width + self.rolesLabel.frame.size.width, self.rolesLabel.center.y);
    
    self.rolesLabel.center = roleStartPos;
    self.atkTypeLabel.center = atkStartPos;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.atkTypeLabel.center = atkOrigin;
    }completion:^(BOOL finished){
        [UIView animateWithDuration:1.0 animations:^{
            self.rolesLabel.center = roleOrigin;
        }];
    }];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
