//
//  HeroesTableViewCell.h
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-04.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeroesTableViewCell : UITableViewCell
@property (nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic) IBOutlet UILabel *heroTypeLabel;

@end
