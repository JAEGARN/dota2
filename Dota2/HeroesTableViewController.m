//
//  HeroesTableViewController.m
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-04.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "HeroesTableViewController.h"
#import "HeroesTableViewCell.h"
#import "AllInfo.h"
#import "HeroDetailViewController.h"
@interface HeroesTableViewController ()

@property NSArray *data;
@property NSArray *searchResult;
@property AllInfo *sharedInfo;
@property NSString *tempName;
@property UIImage* defaultImage;

@end

@implementation HeroesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.defaultImage = [UIImage imageNamed:@"onbbu"];
    self.sharedInfo = [AllInfo infoManager];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    tableView.backgroundColor = [UIColor blackColor];
    
    if (tableView == self.tableView) {
        return self.sharedInfo.allHeroNames.count;
    }else{
        return self.searchResult.count;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   HeroesTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"HeroCell" forIndexPath:indexPath];
    
    tableView.backgroundColor = [UIColor blackColor];
    
    if(tableView == self.tableView){
        self.data = self.sharedInfo.allHeroNames;
    }else{
        self.data = self.searchResult;
    }
    
    cell.nameLabel.text = self.data[indexPath.row];
    // Configure the cell...
    
    return cell;
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText; {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    self.searchResult = [self.sharedInfo.allHeroNames filteredArrayUsingPredicate:predicate];
    [self.tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HeroesTableViewCell *cell = (HeroesTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    self.tempName = cell.nameLabel.text;
    [self performSegueWithIdentifier: @"toHeroDetailSegue" sender:self];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    HeroDetailViewController *detail = [segue destinationViewController];
    
    NSString *imgName = [self.tempName stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    NSString *url = [NSString stringWithFormat:@"http://cdn.dota2.com/apps/dota2/images/heroes/%@_lg.png",[imgName lowercaseString]];
    
    NSURL *itemImage = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:itemImage];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *img = [UIImage imageWithData:data];
            if(img){
                [detail.heroImage setImage:img];
            }else {
                [detail.heroImage setImage:self.defaultImage];
            }
            
            NSDictionary *heroLabel = self.sharedInfo.theHeroes[[self.tempName lowercaseString]];
           
            NSArray *roles = heroLabel[@"roles_l"];
            
            detail.title = self.tempName;
            if (heroLabel[@"bio"]) {
                detail.bioTextView.text = heroLabel[@"bio"];
            }else{
                detail.bioTextView.text = @"Bio not found";
            }
            if (heroLabel[@"atk_l"]) {
                detail.atkTypeLabel.text = heroLabel[@"atk_l"];
            }else{
                detail.atkTypeLabel.text = @"Attack type not found";
            }
            if ([roles componentsJoinedByString:@", "]) {
                detail.rolesLabel.text = [roles componentsJoinedByString:@", "];
            }else{
                detail.rolesLabel.text = @"Roles not found";
            }
        });
    }];
    
    [task resume];
    
}


@end
