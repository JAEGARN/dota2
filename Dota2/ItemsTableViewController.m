//
//  ItemsTableViewController.m
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-04.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "ItemsTableViewController.h"
#import "ItemsTableViewCell.h"
#import "AllInfo.h"
#import "ItemDetailViewController.h"

@interface ItemsTableViewController ()

@property NSArray *data;
@property NSArray *searchResult;
@property AllInfo *sharedInfo;
@property NSString *tempName;
@property UIImage* defaultImage;
@end

@implementation ItemsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sharedInfo = [AllInfo infoManager];
    self.defaultImage = [UIImage imageNamed:@"onbbu"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    tableView.backgroundColor = [UIColor blackColor];
    
    if (tableView == self.tableView) {
        return self.sharedInfo.allItemNames.count;
    }else{
        return self.searchResult.count;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ItemsTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ItemCell" forIndexPath:indexPath];
    
    if(tableView == self.tableView){
        self.data = self.sharedInfo.allItemNames;
    }else{
        self.data = self.searchResult;
    }
    
    cell.itemNameLabel.text = self.data[indexPath.row];
    
    // Configure the cell...
    
    return cell;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText; {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    self.searchResult = [self.sharedInfo.allItemNames filteredArrayUsingPredicate:predicate];
    [self.tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ItemsTableViewCell *cell = (ItemsTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    self.tempName = cell.itemNameLabel.text;
    
    [self performSegueWithIdentifier: @"toItemDetailSegue" sender:self];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ItemDetailViewController *detail = [segue destinationViewController];

    NSDictionary *trueItem;
    
    for (NSString *itemName in self.sharedInfo.theItems) {
        NSDictionary *item = self.sharedInfo.theItems[itemName];
        
        if (item[@"dname"] == self.tempName) {
            trueItem = self.sharedInfo.theItems[itemName];
            break;
        }
    }
    
    NSString *url = [NSString stringWithFormat:@"http://cdn.dota2.com/apps/dota2/images/items/%@", trueItem[@"img"]];
    
    NSURL *itemImage = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:itemImage];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *img = [UIImage imageWithData:data];
            if(img){
                [detail.itemImage setImage:img];
            }else {
                [detail.itemImage setImage:self.defaultImage];
            }
            
            for (NSString *itemName in self.sharedInfo.theItems) {
                NSDictionary *item = self.sharedInfo.theItems[itemName];
                
                if (item[@"dname"] == self.tempName) {
                    NSDictionary *itemInfo = self.sharedInfo.theItems[itemName];
                    
                    if(itemInfo[@"mc"]){
                        detail.mcLabel.text = [itemInfo[@"mc"] stringValue];
                    }else {
                        detail.mcLabel.text = @"Mana cost n/a";
                    }
                    
                    if(itemInfo[@"cost"]){
                        detail.costLabel.text = [itemInfo[@"cost"] stringValue];
                    }else {
                        detail.costLabel.text = @"Cost n/a";
                    }
                    
                    if(itemInfo[@"cd"]){
                        detail.cdLabel.text = [itemInfo[@"cd"] stringValue];
                    }else {
                        detail.cdLabel.text = @"Cooldown  n/a";
                    }
                    
                    if (itemInfo[@"lore"]) {
                        detail.headLabel.text = itemInfo[@"lore"];
                    }else {
                        detail.headLabel.text = @"Header n/a";
                    }
                    
                    if(itemInfo[@"notes"]){
                        NSString *tempString = @"";
                        NSString *tempComp = @"";
                        
                        id components = itemInfo[@"components"];
                        
                        if (components && components != [NSNull null]) {
                            for(NSDictionary *key in components){
                                tempComp = [tempComp stringByAppendingString: [NSString stringWithFormat:@"%@, ", key]];
                            }
                            tempComp = [tempComp stringByReplacingOccurrencesOfString:@"_" withString:@" "];
                        }else {
                            tempComp = @"Components n/a";
                        }
                        
                        tempString = [tempString stringByAppendingString: [NSString stringWithFormat:@"%@\n\nComponents: %@\n\n%@", itemInfo[@"notes"], tempComp, itemInfo[@"desc"] ]];
                        detail.infoText.text = tempString;
                    }else {
                        detail.infoText.text = @"Info n/a";
                    }
                    break;
                }
            }
        });
    }];
    
    [task resume];
    
}


@end
