//
//  MediaViewController.m
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-09.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "MediaViewController.h"

@interface MediaViewController ()

@end

@implementation MediaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self setUpAndStartMediaPlayer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpAndStartMediaPlayer {
   
    NSString *mString = @"http://www.nasa.gov/multimedia/nasatv/NTV-Public-IPS.m3u8";
    NSURL *mUrl = [NSURL URLWithString:mString];
    
    MPMoviePlayerViewController *mPlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:mUrl];
    
    [self presentMoviePlayerViewControllerAnimated:mPlayer];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
