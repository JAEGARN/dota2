//
//  MenuViewController.h
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-05.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AllInfo.h"

@interface MenuViewController : UIViewController

@end
