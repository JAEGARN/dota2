//
//  MenuViewController.m
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-05.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController ()

@property AllInfo *sharedInfo;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sharedInfo = [AllInfo infoManager];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)steamIDButton:(id)sender {
    UIAlertView *alertViewSteamID=[[UIAlertView alloc]initWithTitle:@"Steam ID" message:@"Enter your steam ID, Mine is: U:1:124068. Tyvärr har steam stängt ned funktionen för att kolla repriser men matcherna laddas in och filmspelaren funkar. När du trycker på en match nu så får du se NASA" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Enter", nil];
    alertViewSteamID.alertViewStyle=UIAlertViewStylePlainTextInput;
    alertViewSteamID.delegate = self;
    [alertViewSteamID show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1){
        NSString *steamId = [[alertView textFieldAtIndex:0] text];
        NSLog(@"%@",steamId);
        
        NSString *url = [NSString stringWithFormat:@"https://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/V001/?key=C283FD758242A9DA97348E9BF3958CDB&account_id=%@",steamId];
        
        NSURL *matchApi = [NSURL URLWithString:url];
        NSURLRequest *request = [NSURLRequest requestWithURL:matchApi];
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if(error) {
                NSLog(@"Error: %@", error);
                return;
            }
            
            NSError *parseError;
            NSDictionary *tempMatches = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //int counter = 0;
                NSDictionary *temp2 = tempMatches[@"result"];
                self.sharedInfo.theMatches = temp2[@"matches"];
                
                [self performSegueWithIdentifier: @"playerMatchSegue" sender:self];
            });
        }];
        
        [task resume];
    }
}

- (IBAction)mediaButton:(id)sender {
    NSString *mString = @"http://www.nasa.gov/multimedia/nasatv/NTV-Public-IPS.m3u8";
    
    NSURL *mUrl = [NSURL URLWithString:mString];
    
    MPMoviePlayerViewController *mPlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:mUrl];
    
    [self presentMoviePlayerViewControllerAnimated:mPlayer];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
