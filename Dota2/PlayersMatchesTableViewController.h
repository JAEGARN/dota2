//
//  PlayersMatchesTableViewController.h
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-16.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "allInfo.h"
#import <MediaPlayer/MediaPlayer.h>

@interface PlayersMatchesTableViewController : UITableViewController

@end
