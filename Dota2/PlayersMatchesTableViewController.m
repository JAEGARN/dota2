//
//  PlayersMatchesTableViewController.m
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-16.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "PlayersMatchesTableViewController.h"

@interface PlayersMatchesTableViewController ()

@property AllInfo *sharedInfo;
@property NSMutableArray *matchIDs;

@end

@implementation PlayersMatchesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sharedInfo = [AllInfo infoManager];
    self.matchIDs = [[NSMutableArray alloc] init];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.sharedInfo.theMatches.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"matchesCell" forIndexPath:indexPath];
    
    cell.textLabel.textColor = [UIColor whiteColor];
    
    for(NSDictionary *match in self.sharedInfo.theMatches){
        [self.matchIDs addObject:match[@"match_id"]];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"Match ID: %@", self.matchIDs[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *url = [NSString stringWithFormat:@"https://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/V001/?key=C283FD758242A9DA97348E9BF3958CDB&match_id=%@",self.matchIDs[indexPath.row]];
    
    NSURL *matchApi = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:matchApi];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *parseError;
        NSDictionary *matchDetail = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *temp = matchDetail[@"result"];
            
            NSString *mString = @"http://www.nasa.gov/multimedia/nasatv/NTV-Public-IPS.m3u8";
            NSURL *mUrl = [NSURL URLWithString:mString];
            
            MPMoviePlayerViewController *mPlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:mUrl];
            
            [self presentMoviePlayerViewControllerAnimated:mPlayer];
        });
    }];
    
    [task resume];

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
