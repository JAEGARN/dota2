//
//  ViewController.m
//  Dota2
//
//  Created by Viktor Jegerås on 2015-04-04.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "ViewController.h"
#import "AllInfo.h"

@interface ViewController ()


@property AllInfo *sharedInfo;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sharedInfo = [AllInfo infoManager];
    
    self.sharedInfo.allHeroNames = [[NSMutableArray alloc] init];
    self.sharedInfo.allItemNames = [[NSMutableArray alloc] init];
    
    [self loadHeroes];
    [self loadItems];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadHeroes {
    //Nytt API(bättre?) http://www.dota2.com/jsfeed/heropickerdata
    //Gamla https://api.steampowered.com/IEconDOTA2_570/GetHeroes/v0001/?key=C283FD758242A9DA97348E9BF3958CDB&language=en_us
    
    NSURL *heroApi = [NSURL URLWithString:@"http://www.dota2.com/jsfeed/heropickerdata?lang=en_us"];
    NSURLRequest *request = [NSURLRequest requestWithURL:heroApi];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *parseError;
        
        self.sharedInfo.theHeroes = [NSJSONSerialization JSONObjectWithData:data
                                                     options:kNilOptions error:&parseError];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            for (NSString *heroName in self.sharedInfo.theHeroes) {
                NSDictionary *hero = self.sharedInfo.theHeroes[heroName]; 
               
                [self.sharedInfo.allHeroNames addObject:hero[@"name"]];
            }
                       
        });
    }];
    
    [task resume];

}

-(void)loadItems {
    NSURL *itemApi = [NSURL URLWithString:@"http://www.dota2.com/jsfeed/itemdata"];
    NSURLRequest *request = [NSURLRequest requestWithURL:itemApi];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *parseError;
        NSDictionary *items = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        
        self.sharedInfo.theItems = items[@"itemdata"];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            for (NSString *itemName in self.sharedInfo.theItems) {
                NSDictionary *item = self.sharedInfo.theItems[itemName];
                
                [self.sharedInfo.allItemNames addObject:item[@"dname"]];
            }
            
            [self performSegueWithIdentifier: @"menuSegue" sender:self];
        });
    }];
    
    [task resume];

}

@end
